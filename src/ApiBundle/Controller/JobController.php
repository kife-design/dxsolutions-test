<?php
namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpKernel\Exception\HttpException;

class JobController extends FOSRestController
{
    /**
     * @param $job
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getJobAction($job)
    {
        $em = $this->getDoctrine()->getManager();
        $job = $em->getRepository('AppBundle:Job')->find($job);

        if(! $job) {
            throw new HttpException(404, "Job " . $job . " not found.");
        }

        $view = $this->view($job);
        return $this->handleView($view);
    }

    public function getJobsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $jobs = $em->getRepository('AppBundle:Job')->findAll();

        $view = $this->view($jobs);
        return $this->handleView($view);
    }
}
