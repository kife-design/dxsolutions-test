<?php

namespace AppBundle\Controller;

use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Job;

/**
 * Job controller.
 *
 */
class JobController extends Controller
{
    /**
     * Lists all Job entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $jobs = $em->getRepository('AppBundle:Job')->findAll();

        return $this->render(
            'job/index.html.twig',
            [
                'jobs' => $jobs,
            ]
        );
    }

    /**
     * Creates a new Job entity.
     *
     */
    public function newAction()
    {
        return $this->render('job/new.html.twig');
    }

    public function storeAction(Request $request)
    {
        $job = new Job();
        $job->setName($request->get('name'));
        $job->setDescription($request->get('description'));
        $job->setAddress($request->get('address'));
        $job->setZipcode($request->get('zipcode'));
        $job->setCity($request->get('city'));
        $job->setCreatedAt(new DateTime());

        $em = $this->getDoctrine()->getManager();
        $em->persist($job);
        $em->flush();

        return $this->redirectToRoute('jobs_show', ['id' => $job->getId()]);
    }

    /**
     * Finds and displays a Job entity.
     * @param Job $job
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Job $job)
    {
        $deleteForm = $this->createDeleteForm($job);

        return $this->render(
            'job/show.html.twig',
            [
                'job'         => $job,
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Displays a form to edit an existing Job entity.
     * @param Request $request
     * @param Job     $job
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Job $job)
    {
        return $this->render(
            'job/edit.html.twig',
            [
                'job' => $job,
            ]
        );
    }

    public function updateAction(Request $request, Job $job)
    {
        $job->setName($request->get('name'));
        $job->setDescription($request->get('description'));
        $job->setAddress($request->get('address'));
        $job->setZipcode($request->get('zipcode'));
        $job->setCity($request->get('city'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($job);
        $em->flush();

        return $this->redirectToRoute('jobs_show', ['id' => $job->getId()]);
    }

    /**
     * Deletes a Job entity.
     *
     */
    public function deleteAction(Request $request, Job $job)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($job);
        $em->flush();

        return $this->redirectToRoute('jobs_index');
    }

    /**
     * Creates a form to delete a Job entity.
     *
     * @param Job $job The Job entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Job $job)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('jobs_delete', ['id' => $job->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
