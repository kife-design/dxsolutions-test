var jobs = {
    initMap: function () {
        if ($('#map').length > 0) {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 10,
                center: {lat: -34.397, lng: 150.644}
            });
            var geocoder = new google.maps.Geocoder();

            var address = $('#map').attr('data-geocode');
            geocoder.geocode({'address': address}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });
                } else {
                    $('.invalid-data').removeClass('hidden');
                }
            });
        }
    },

    initTinymce: function () {
        tinymce.init({
            selector: '#description',
            menubar: false,
            min_height: 200,
            toolbar: 'bold italic underline strikethrough | alignleft aligncenter alignright | bullist numlist | code',
        });
    }
}
